
extends Node2D

# member variables here, example:
# var a=2
# var b="textvar"

var speed=2

func _process(delta):
	if(get_node("message").get_opacity()==0):
		return
	get_node("message").set_opacity(get_node("message").get_opacity()-speed*delta)

func reset():
	get_node("message").set_opacity(1.0)

func _ready():
	# Initalization here
	get_node("message").set_opacity(0)
	set_process(true)
	pass


