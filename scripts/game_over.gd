
extends Node2D

# member variables here, example:
# var a=2
# var b="textvar"
var m_screen
var m_mode
var kMovingSpeed = 5
const MODE = { "CONGRATULATIONS":0, "GAME_OVER":1 }

func moving_title():
	var pos = get_node("text").get_pos()
	pos.x += kMovingSpeed
	if (pos.x > m_screen.size.x):
	 	pos.x = -get_node("text").get_size().x
	get_node("text").set_pos(pos)

func _process(delta):
	if(m_mode == MODE.GAME_OVER):
		moving_title()
	else:
		pass

func game_over():
	get_node("text").set_text("GAME OVER")
	m_mode = MODE.GAME_OVER
	get_node("share").set_opacity(0)

func succeed():
	get_node("text").set_text("CONGRATULATIONS")
	m_mode = MODE.CONGRATULATIONS
	get_node("share").set_opacity(1)
	get_node("ElapsedTime").set_opacity(1)

func setup_graphics():
	m_screen = get_viewport_rect()
	var position = get_node("text").get_pos()
	position.x = (m_screen.size.x - get_node("text").get_size().width)/2
	get_node("text").set_pos(position)

	position = get_node("restart").get_pos()
	position.x = (m_screen.size.x - get_node("restart").get_size().width)/2
	get_node("restart").set_pos(position)
	
	position = get_node("share").get_pos()
	position.x = (m_screen.size.x - get_node("share").get_size().width)/2
	get_node("share").set_pos(position)
	
	position = get_node("ElapsedTime").get_pos()
	position.x = (m_screen.size.x - get_node("ElapsedTime").get_size().width)/2
	get_node("ElapsedTime").set_pos(position)
	

func _ready():
	setup_graphics()
	play()

func stop():
	set_process(false)

func play():
	set_process(true)
