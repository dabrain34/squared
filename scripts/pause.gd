
extends Node2D

# member variables here, example:
# var a=2
# var b="textvar"

var m_screen

func setup_graphics():
	m_screen = get_viewport_rect()
	var position = get_node("globalParams").get_pos()
	position.x = (m_screen.size.x - get_node("globalParams").get_size().width)/2
	get_node("globalParams").set_pos(position)

func _ready():
	setup_graphics()
	get_node("globalParams/Statistics/number").set_text(str(get_parent().get_succeed_games()))

func set_time_elapsed(time_elapsed):
	var second_elapsed = time_elapsed%60
	var minute_elapsed = time_elapsed/60
	var min_time_str = str("%02d" % minute_elapsed) 
	var second_time_str = str("%02d" % second_elapsed)
	get_node("globalParams/TimeElapsed/Minutes").set_text(min_time_str)
	get_node("globalParams/TimeElapsed/Seconds").set_text(second_time_str)


func _on_orange_btn_pressed():
	clearCircles()
	get_node("globalParams/circles/orange/circle").set_opacity(1)
	get_parent().changeToOrange()

func _on_yellow_btn_pressed():
	clearCircles()
	get_node("globalParams/circles/yellow/circle").set_opacity(1)
	get_parent().changeToYellow()

func _on_green_btn_pressed():
	clearCircles()
	get_node("globalParams/circles/green/circle").set_opacity(1)
	get_parent().changeToGreen()

func _on_blue_btn_pressed():
	clearCircles()
	get_node("globalParams/circles/blue/circle").set_opacity(1)
	get_parent().changeToBlue()

func _on_violet_btn_pressed():
	clearCircles()
	get_node("globalParams/circles/violet/circle").set_opacity(1)
	get_parent().changeToViolet()

func _on_darkGreen_btn_pressed():
	clearCircles()
	get_node("globalParams/circles/darkGreen/circle").set_opacity(1)
	get_parent().changeToDarkGreen()

func clearCircles():
	get_node("globalParams/circles/orange/circle").set_opacity(0)
	get_node("globalParams/circles/yellow/circle").set_opacity(0)
	get_node("globalParams/circles/green/circle").set_opacity(0)
	get_node("globalParams/circles/blue/circle").set_opacity(0)
	get_node("globalParams/circles/violet/circle").set_opacity(0)
	get_node("globalParams/circles/darkGreen/circle").set_opacity(0)