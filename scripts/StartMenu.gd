
extends Node2D

# member variables here, example:
# var a=2
# var b="textvar"
var pause
var game
var credits
var polygon_background
var orangeColor = Color(0.878431,0.443137,0.294118)
var yellowColor = Color(0.905882,0.772549,0.34902)
var greenColor = Color(0.247059,0.611765,0.556863)
var blueColor = Color(0.160784,0.27451,0.329412)
var violetColor = Color(0.329412,0.223529,0.25098)
var darkGreenColor = Color(0.282353,0.388235,0.309804)
var blackColor = Color(0,0,0)
var m_screen
var m_backgroundColor
var m_oldBackgroundColor
var m_succeedGames
var m_configFile
var m_configPath = "user://squared_config.txt"


func _process(delta):
	var opacity = get_node("TITLE/SQUARED_TITLE").get_opacity()-0.02
	if(opacity > 0):
		get_node("TITLE/SQUARED_TITLE").set_opacity(opacity)
	else:
		start_game()
		set_process(false)
		get_node("TITLE/SQUARED_TITLE").set_opacity(0)

func draw_background(color):
	var polygone_array = Vector2Array()
	polygone_array.push_back(Vector2(0,0))
	polygone_array.push_back(Vector2(m_screen.size.x,0))
	polygone_array.push_back(Vector2(m_screen.size.x,m_screen.size.y))
	polygone_array.push_back(Vector2(0,m_screen.size.y))
	polygon_background = Polygon2D.new()
	polygon_background.set_polygon(polygone_array)
	polygon_background.set_color(color)
	get_node("background").add_child(polygon_background)

func changeColor(color):
	get_node("background").remove_child(polygon_background)
	draw_background(color)
	m_oldBackgroundColor = m_backgroundColor
	m_backgroundColor = color

func changeToPreviousColor():
	changeColor(m_oldBackgroundColor)

func changeToOrange():
	changeColor(orangeColor)

func changeToYellow():
	changeColor(yellowColor)

func changeToGreen():
	changeColor(greenColor)

func changeToBlue():
	changeColor(blueColor)

func changeToViolet():
	changeColor(violetColor)

func changeToDarkGreen():
	changeColor(darkGreenColor)

func changeToBlack():
	changeColor(blackColor)

func setup_graphics():
	m_screen = get_viewport_rect()
	var title_pos = get_node("TITLE").get_pos()
	title_pos.x = (m_screen.size.x - get_node("TITLE").get_size().width)/2
	get_node("TITLE").set_pos(title_pos)
	m_backgroundColor = orangeColor
	m_oldBackgroundColor = m_backgroundColor
	draw_background(m_backgroundColor)

func loadConfig():
	var config = ConfigFile.new()

	var err = config.load(m_configPath)
	if err:
		m_succeedGames = 0
	else:
		m_succeedGames = config.get_value("SCORE","SUCCESS",0)

func saveConfig():
	var config = ConfigFile.new()
	var err = config.load(m_configPath)
	config.set_value("SCORE","SUCCESS",m_succeedGames)
	config.save(m_configPath)

func _ready():
	# Initalization here
	loadConfig()
	get_node("TITLE/SQUARED_TITLE").set_opacity(1)
	set_process(true)
	setup_graphics()

func start_game():
	game = load("res://scenes/grid.tscn").instance()
	game.get_node("Footer/pause_btn").connect("pressed",self,"_pause_game",Array(), CONNECT_DEFERRED)
	add_child(game)
	game.setup(self,5)

func _on_START_pressed():
	start_game()
	pass # replace with function body

func _pause_game():
	pause = load("res://scenes/pause.tscn").instance()
	add_child(pause)
	game.pause(true)
	pause.get_node("globalParams/restart").connect("pressed",self,"_on_restart",Array(), CONNECT_DEFERRED)
	pause.get_node("globalParams/continue").connect("pressed",self,"_on_continue",Array(), CONNECT_DEFERRED)
	pause.set_time_elapsed(game.get_time_elapsed())

func _on_continue():
	remove_child(pause)
	game.pause(false)

func _on_restart():
	_on_continue()
	_exit_game()

func _exit_game():
	remove_child(game)
	get_node("TITLE/SQUARED_TITLE").set_opacity(1)
	set_process(true)

func get_succeed_games():
	return m_succeedGames

func incrementSuccess():
	m_succeedGames += 1
	saveConfig()
