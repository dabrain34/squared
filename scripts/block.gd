
extends Node2D

# member variables here, example:
# var a=2
# var b="textvar"

var speed = 10
var m_max_pos = Vector2(0,0)
var m_row=0
var m_col=0
var m_used=false
var m_selected = false
var m_max_alpha = 0.1

const MODE = { "IDLE":0,
			   "MOVING_TO_POS":1,
			   "APPEARING_IN_POS":2,
			   "SELECTING":3,
			   "SELECTABLE":4,
			   "BLINKING":5
			 }
var m_animation_mode = MODE.IDLE
var m_selected_opacity = 0
var m_selectionBlinkWay = 1

func stop():
	set_process(false)

func play():
	set_process(true)

func _process(delta):
	if m_animation_mode == MODE.APPEARING_IN_POS:
		appear_in_pos(delta)
	elif m_animation_mode == MODE.MOVING_TO_POS:
		move_to_pos(delta)
	elif m_animation_mode == MODE.SELECTING:
		selecting(delta)
	elif m_animation_mode == MODE.SELECTABLE:
		selectable(delta)
	elif m_animation_mode == MODE.BLINKING:
		blinking(delta)
	elif m_animation_mode == MODE.IDLE:
		stop()

func selecting(delta):
	#print("selecting")
	m_selected_opacity += 0.1
	if m_selected_opacity < 1:
		get_node("block_control/block").set_opacity(m_selected_opacity)
		get_node("block_control/white").set_opacity(m_selected_opacity)
	else:
		m_animation_mode= MODE.IDLE

func selectable(delta):
	#print("selectable")
	m_selected_opacity += 0.015 * m_selectionBlinkWay
	if m_selectionBlinkWay == 1:
		if m_selected_opacity >= 1:
			m_selectionBlinkWay = -1
			m_selected_opacity = 1
	else:
		if m_selected_opacity <= 0.2:
			m_selectionBlinkWay = 1
			m_selected_opacity = 0.2
	get_node("border").set_opacity(m_selected_opacity)

func blinking(delta):
	print("blinking")
	m_selected_opacity += 0.05 * m_selectionBlinkWay
	if m_selectionBlinkWay == 1:
		if m_selected_opacity >= 1:
			m_selectionBlinkWay = -1
			m_selected_opacity = 1
	else:
		if m_selected_opacity <= 0.2:
			m_selectionBlinkWay = 1
			m_selected_opacity = 0.2

	get_node("block_control/white").set_opacity(m_selected_opacity)

func move_to_pos(delta):
	var pos = get_pos()
	pos.x += speed
	pos.y += speed
	set_pos(pos)
	get_node("black").set_opacity(m_max_alpha)
	if(pos.x >= m_max_pos.x && pos.y >= m_max_pos.y ):
		set_pos(m_max_pos)
		m_animation_mode = MODE.IDLE

func appear_in_pos(delta):
	set_pos(m_max_pos)
	var opacity = get_node("black").get_opacity()
	opacity = opacity + (randi()%1000)/float(50000)
	#print("opacity=",opacity)
	get_node("black").set_opacity(opacity)
	if(opacity > m_max_alpha):
		get_node("black").set_opacity(m_max_alpha)
		m_animation_mode = MODE.IDLE

func is_used():
	return m_used

func set_selectable(value):
	m_selected = value
	if(value):
		get_node("border").set_opacity(1)
		m_animation_mode = MODE.SELECTABLE
		play()
	else:
		if m_animation_mode == MODE.SELECTABLE:
			m_animation_mode = MODE.IDLE
			stop()
		get_node("border").set_opacity(0)

func set_blinking():
	m_selected_opacity = (randi()%10)/float(10)
	m_animation_mode = MODE.BLINKING
	get_node("block_control/block").set_opacity(0)
	get_node("border").set_opacity(0)
	play()

func is_selected():
	return m_selected

func setup(row,col,min_pos,max_pos):
	set_pos(min_pos)
	m_max_pos = max_pos
	m_row = row
	m_col = col
	m_used = false
	m_animation_mode = MODE.APPEARING_IN_POS
	get_node("block_control/block").set_text("")
	set_selectable(false)
	play()

func _ready():
	get_node("black").set_opacity(0)
	get_node("block_control/white").set_opacity(0)
	# Initalization here
	pass

func set_selected(value_str):
	get_node("block_control/block").set_text(value_str)
	m_used=true
	m_selected_opacity = 0
	m_animation_mode = MODE.SELECTING
	play()


func _on_block_pressed():
	get_parent().get_parent().process_block(self)
	pass # replace with function body
