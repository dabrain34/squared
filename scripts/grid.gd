
extends Node2D

# member variables here, example:
# var a=2
# var b="textvar"

var m_screen
var m_grid_size = 5
var block_elements
var BLOCK_SIZE=50
var m_initial_pos
var SPACE = 10
var MARGIN_X = 70
var time_elapsed = 0
var time_str

var m_game_over
var base_menu

var m_currentValue = 1
var m_last_block = null
var MAX_INIT_POS=4
var m_timer_started=false
var m_game_started=false

var m_solution = [  Vector2(0,0),
					Vector2(0,3),
					Vector2(2,1),
					Vector2(2,4),
					Vector2(0,2),
					Vector2(3,2),
					Vector2(1,0),
					Vector2(1,3),
					Vector2(4,3),
					Vector2(4,0),
					Vector2(2,2),
					Vector2(0,4),
					Vector2(3,4),
					Vector2(3,1),
					Vector2(0,1),
					Vector2(2,3),
					Vector2(4,1),
					Vector2(4,4),
					Vector2(1,4),
					Vector2(1,1),
					Vector2(3,3),
					Vector2(3,0),
					Vector2(1,2),
					Vector2(4,2),
					Vector2(2,0),
				 ]

var m_wrong = [  Vector2(0,0),
					Vector2(0,3),
					Vector2(3,3),
					Vector2(3,0),
					Vector2(1,2),
					Vector2(3,2),
					Vector2(3,4),
					Vector2(3,1),
					Vector2(0,1),
					Vector2(0,4),
					Vector2(2,2),
					Vector2(4,0),
					Vector2(4,3),
					Vector2(1,3),
					Vector2(1,0),
					Vector2(3,2),
					Vector2(1,4),
					Vector2(1,1),
					Vector2(4,1),
					Vector2(2,3),
					Vector2(2,0),
					Vector2(0,2),
					Vector2(2,4),
				 ]

func valid_block(block,last_block):
	print("block",block.m_row,block.m_col)
	print("last_block",last_block.m_row,last_block.m_col)
	if (block.is_used()==true):
		return false
	if(abs(block.m_row -m_last_block.m_row) == 3 &&  block.m_col == m_last_block.m_col):
		print("cas1")
		return true
	if(abs(block.m_col -m_last_block.m_col) == 3 &&  block.m_row == m_last_block.m_row):
		print("cas2")
		return true
	if(abs(block.m_col -m_last_block.m_col) == 2 &&  abs(block.m_row -m_last_block.m_row) == 2):
		print("cas3")
		return true
	return false

func valid_next_move(block):
	if(is_block_available(block.m_row+3,block.m_col)==true):
		return true
	if(is_block_available(block.m_row-3,block.m_col)==true):
		return true
	if(is_block_available(block.m_row,block.m_col+3)==true):
		return true
	if(is_block_available(block.m_row,block.m_col-3)==true):
		return true
	if(is_block_available(block.m_row+2,block.m_col+2)==true):
		return true
	if(is_block_available(block.m_row-2,block.m_col+2)==true):
		return true
	if(is_block_available(block.m_row+2,block.m_col-2)==true):
		return true
	if(is_block_available(block.m_row-2,block.m_col-2)==true):
		return true
	return false



func finish(succeed):
	m_game_over = load("res://scenes/game_over.tscn").instance()
	m_game_over.get_node("restart").connect("pressed",self,"_restart_game",Array(), CONNECT_DEFERRED)
	if (succeed):
		get_node("Header").set_opacity(1)
		get_node("Grid").set_opacity(0.5)
		get_node("Footer").set_opacity(0)
		set_blocks_blink()
		m_game_over.succeed()
		get_parent().incrementSuccess()
	else:
		set_opacity(0)
		m_game_over.game_over()
	# m_game_over.set_pos(m_initial_pos)
	
	m_timer_started =false
	get_parent().add_child(m_game_over)

func _restart_game():
	set_opacity(1)
	get_node("Header").set_opacity(1)
	get_node("Grid").set_opacity(1)
	get_node("Footer").set_opacity(1)
	#base_menu.set_opacity(1)
	m_game_over.stop()
	get_parent().remove_child(m_game_over)
	get_parent().changeToPreviousColor()
	base_menu._exit_game()

func process_block(block):
	if(m_last_block==null || valid_block(block,m_last_block)==true):
		block.set_selected(str(m_currentValue))
		m_currentValue+=1
		m_last_block = block
		m_game_started = true
		m_timer_started = true
		select_next_available_move()
	
	if (m_currentValue > pow(m_grid_size,2)):
		finish(true)
	elif(valid_next_move(block)==false):
		finish(false)

func set_blocks_blink():
	print("set blocks blink")
	for i in range(0,m_grid_size):
		for j in range(0,m_grid_size):
			var block = get_block(i,j)
			block.set_blinking()

func select_next_available_move():
	print("select_next_available_move")
	for i in range(0,m_grid_size):
		for j in range(0,m_grid_size):
			var block = get_block(i,j)
			if(!block.is_used()):
				block.set_selectable(valid_block(block,m_last_block))


func get_block(row,col):
	print("row:",row," col:", col)
	if (row < m_grid_size && row >=0 && col < m_grid_size && col >=0):
		return block_elements[row][col]
	else:
		return null
	
func is_block_available(row,col):
	var block = get_block(row,col)
	if(block != null):
		return !block.is_used()
	else:
		return false

func get_initial_pos(value):
	var initial_pos = Vector2(0,0)
	if(value == 0):
		initial_pos.x = (m_screen.size.x - BLOCK_SIZE*m_grid_size)/2
		initial_pos.y = (m_screen.size.y - BLOCK_SIZE*m_grid_size)/2
	elif (value == 1):
		initial_pos.x = (m_screen.size.x - BLOCK_SIZE*m_grid_size)/2 + BLOCK_SIZE*m_grid_size
		initial_pos.y = (m_screen.size.y - BLOCK_SIZE*m_grid_size)/2
	elif (value == 2):
		initial_pos.x = (m_screen.size.x - BLOCK_SIZE*m_grid_size)/2
		initial_pos.y = (m_screen.size.y - BLOCK_SIZE*m_grid_size)/2 + BLOCK_SIZE*m_grid_size
	elif (value == 3):
		initial_pos.x = (m_screen.size.x - BLOCK_SIZE*m_grid_size)/2 + BLOCK_SIZE*m_grid_size
		initial_pos.y = (m_screen.size.y - BLOCK_SIZE*m_grid_size)/2 + BLOCK_SIZE*m_grid_size
	return initial_pos

func setup_graphics():
	m_screen = get_viewport_rect()
	var timer_pos = get_node("Header/GlobalTimer").get_pos()
	timer_pos.x = (m_screen.size.x - get_node("Header/GlobalTimer").get_size().width)/2
	get_node("Header/GlobalTimer").set_pos(timer_pos)
	
	var logo_pos = get_node("Footer/logo").get_pos()
	logo_pos.x = (m_screen.size.x - get_node("Footer/logo").get_size().width)/10
	get_node("Footer/logo").set_pos(logo_pos)

func setup(parent,size):
	block_elements = []
	m_timer_started = false
	m_game_started = false
	m_currentValue = 1
	base_menu = parent
	setup_graphics()

	m_grid_size = size
	m_initial_pos = get_initial_pos(0)
	m_initial_pos.y = get_node("Header/GlobalTimer").get_pos().y + get_node("Header/GlobalTimer").get_size().y 
	print(m_initial_pos)
	var space_x = SPACE
	var space_y = SPACE
	m_initial_pos.x = MARGIN_X
	var start_initial_pos = m_initial_pos #randi()%MAX_INIT_POS)

	var block

	var new_block_size = (m_screen.size.x - (m_grid_size-1)*space_x - 2*MARGIN_X)/m_grid_size
	
	print("new_block_size: ", new_block_size)
	var scale = new_block_size/BLOCK_SIZE
	for i in range(0,m_grid_size):
		block_elements.append([])
		for j in range(0,m_grid_size):
			block = load("res://scenes/block.tscn").instance()
			block.setup(i,j,start_initial_pos,Vector2((new_block_size + space_x)*i + m_initial_pos.x ,(new_block_size+ space_y)*j + m_initial_pos.y ))
			block.set_scale(Vector2(scale,scale))
			get_node("Grid").add_child(block)
			block_elements[i].append(block)
	
	var position = get_node("Footer/logo").get_pos()
	position.y = start_initial_pos.y + (new_block_size + space_x)*5
	get_node("Footer/logo").set_pos(position)
	
	position = get_node("Footer/pause_btn").get_pos()
	position.y = start_initial_pos.y + (new_block_size + space_x)*5
	get_node("Footer/pause_btn").set_pos(position)

func _ready():
	pass

func pause(state):
	if(state):
		m_timer_started = false
		set_opacity(0)
	else:
		m_timer_started = true
		set_opacity(1)

func _on_Timer_timeout():
	if(m_game_started && m_timer_started):
		time_elapsed += 1
	var second_elapsed = time_elapsed%60
	var minute_elapsed = time_elapsed/60
	var min_time_str = str("%02d" % minute_elapsed) 
	var second_time_str = str("%02d" % second_elapsed)
	get_node("Header/GlobalTimer/Minutes").set_text(min_time_str)
	get_node("Header/GlobalTimer/Seconds").set_text(second_time_str)

func get_time_elapsed():
	return time_elapsed

func _on_debug_24_pressed():
	for i in range(0, m_solution.size()-1):
		process_block(block_elements[m_solution[i].x][m_solution[i].y])

func _on_debug_22_pressed():
	for i in range(0, m_wrong.size()):
		process_block(block_elements[m_wrong[i].x][m_wrong[i].y])